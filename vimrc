filetype plugin indent on

syntax on
set tabstop=4
set shiftwidth=4
set smarttab
set wrap
set ai   
set et
set cin 
set number
set showmatch
set hlsearch
set incsearch

set listchars=tab:->
set list 


map <F5> :cp<CR>
map <F6> :cn<CR>

nnoremap <F2> "+y
vnoremap <F2> "+y
nnoremap <F3> "+gP
vnoremap <F3> "+gP

autocmd FileType c,cpp,java,systemverilog,verilog autocmd BufWritePre <buffer> %s/\s\+$//e
