syntax on
setlocal tabstop=4
setlocal shiftwidth=4
setlocal noexpandtab
setlocal smarttab
setlocal noet
setlocal wrap
setlocal ai   
setlocal cin 
setlocal number
setlocal showmatch
setlocal hlsearch
setlocal incsearch

setlocal nolist
